<section class="u-clearfix u-white u-section-1" id="sec-4015">
    <div class="u-expanded-width u-palette-2-light-3 u-shape u-shape-rectangle u-shape-1"></div>
    <img class="u-expanded-width-sm u-image u-image-1" src="assets/images/b8415668716720fa2a5e6a4b069ab0b3.jpeg">
    <div class="u-container-style u-expanded-width-sm u-expanded-width-xs u-group u-group-1">
        <div class="u-container-layout u-valign-middle-xs">
            <h1 class="u-align-center u-custom-font u-text u-title u-text-1">
                Business Coach&nbsp;For<br>Women Entrepreneurs
            </h1>
            <p class="u-align-center u-text u-text-2">
                It doesn’t take tons of time or scary talent to be successful. You’ve already got what it takes. Now, you need to tap it. Let’s go!
            </p>
            <div class="u-border-7 u-border-palette-2-dark-2 u-line u-line-horizontal u-line-1"></div>
        </div>
    </div>
    <img src="assets/images/177a0cd3cf8239fd77d0994fde9f6248.png" alt="" class="u-image u-image-default u-image-2" data-image-width="263" data-image-height="241">
</section>