<footer>
    <section class="u-clearfix u-palette-2-light-3 u-section-9" id="sec-9e83">
        <div class="u-clearfix u-sheet u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-0 u-layout-spacing-vertical u-layout-wrap u-layout-wrap-1">
                <div class="u-gutter-0 u-layout">
                    <div class="u-layout-row">

                        <div class="u-size-30">
                            <div class="u-layout-col">
                                <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
                                    <div class="u-container-layout u-valign-middle u-container-layout-1">
                                        &copy; | {date('Y')} {$site_name}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>