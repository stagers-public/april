<meta property="og:title" content="{$modx->resource->longtitle|default:$modx->resource->pagetitle}">
<meta property="og:type" content="website">
<meta property="og:url" content="{$modx->resource->alias}">
<meta property="og:image" content="assets/images/logo.png">
<meta property="og:locale" content="{$locale}">
<meta property="og:description" content="{$modx->resource->description|default:$modx->resource->longtitle}">
<meta property="og:site_name" content="{$site_name}">