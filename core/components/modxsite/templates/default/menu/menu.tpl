{$params = [
    'startId'           => 0,
    'level'             => 3,
    'levelClass'        => false,
    'activeClass'       => 'active'
]}

{processor action='web/menu/getdata' ns=modxsite params=$params assign=result}

<div class="u-container-layout u-valign-middle u-container-layout-1 u-nav-container">
    {foreach $result.object as $object}
        {include 'menu/nav.tpl' nocache}
    {/foreach}
</div>
