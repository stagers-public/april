<!DOCTYPE html>

{$site_name = $modx->getOption('site_name')}
{$site_start = $modx->getOption('site_start')}
{$site_url = $modx->getOption('site_url')}
{$cultureKey = $modx->getOption('cultureKey')}
{$locale = $modx->getOption('locale')}

<html lang="{$cultureKey}" data-src="{if $modx->getOption('modxSmarty.pre_template')}{$modx->getOption('modxSmarty.pre_template')}{else}{$modx->getOption('modxSmarty.template')}{/if}">
<head>
    <title>{$modx->resource->longtitle|default:$modx->resource->pagetitle} | {$site_name|capitalize:true}</title>
    <meta charset="{$locale}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/ico">

    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dancing+Script:400,500,600,700">

    {include 'common/openGraph.tpl'}

    {* Все стили *}
    {include 'common/style.tpl'}
    {block 'custom'}{/block}

</head>
<body>

    {* Навигация *}
    {include 'menu/menu.tpl'}

    {* Заголовок страницы *}
    {block 'header'}{/block}

    {* Основной контент страницы *}
    {block 'content'}
        {$modx->resource->content}
    {/block}

    {* footer *}
    {block 'footer'}
        {include 'common/footer.tpl'}
    {/block}

    {* Все скрипты *}
    {include 'common/script.tpl'}
</body>
</html>