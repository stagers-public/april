<section class="u-clearfix u-palette-2-light-2 u-section-7" id="sec-df48">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-clearfix u-expanded-width u-gutter-20 u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-container-style u-layout-cell u-left-cell u-size-15 u-size-30-md u-layout-cell-1">
                        <div class="u-container-layout u-valign-middle u-container-layout-1">
                            <h1 class="u-align-center u-custom-font u-text u-text-body-alt-color u-title u-text-1" data-animation-name="counter" data-animation-event="scroll" data-animation-duration="3000">72</h1>
                            <p class="u-align-center u-text u-text-body-alt-color u-text-2">Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="u-container-style u-layout-cell u-size-15 u-size-30-md u-layout-cell-2">
                        <div class="u-container-layout u-valign-middle u-container-layout-2">
                            <h1 class="u-align-center u-custom-font u-text u-text-body-alt-color u-title u-text-3" data-animation-name="counter" data-animation-event="scroll" data-animation-duration="3000">52</h1>
                            <p class="u-align-center u-text u-text-body-alt-color u-text-4">Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="u-container-style u-layout-cell u-size-15 u-size-30-md u-layout-cell-3">
                        <div class="u-container-layout u-valign-middle u-container-layout-3">
                            <h1 class="u-align-center u-custom-font u-text u-text-body-alt-color u-title u-text-5" data-animation-name="counter" data-animation-event="scroll" data-animation-duration="3000">10</h1>
                            <p class="u-align-center u-text u-text-body-alt-color u-text-6">Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="u-container-style u-layout-cell u-right-cell u-size-15 u-size-30-md u-layout-cell-4">
                        <div class="u-container-layout u-valign-middle u-container-layout-4">
                            <h1 class="u-align-center u-custom-font u-text u-text-body-alt-color u-title u-text-7" data-animation-name="counter" data-animation-event="scroll" data-animation-duration="3000">95</h1>
                            <p class="u-align-center u-text u-text-body-alt-color u-text-8">Lorem ipsum dolor</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>