<div class="u-size-30">
    <div class="u-layout-row">
        <div class="u-align-left u-container-style u-layout-cell u-right-cell u-size-60 u-layout-cell-3">
            <div class="u-container-layout u-container-layout-3">
                <div class="u-align-left u-expanded u-grey-10 u-map">
                    <div class="embed-responsive">
                        <iframe class="embed-responsive-item" src="//maps.google.com/maps?output=embed&amp;q=Manhattan%2C%20New%20York&amp;z=10&amp;t=m" data-map="JTdCJTIyYWRkcmVzcyUyMiUzQSUyMk1hbmhhdHRhbiUyQyUyME5ldyUyMFlvcmslMjIlMkMlMjJ6b29tJTIyJTNBMTAlMkMlMjJ0eXBlSWQlMjIlM0ElMjJyb2FkJTIyJTJDJTIybGFuZyUyMiUzQSUyMiUyMiU3RA=="></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>