<section class="u-clearfix u-section-3" id="sec-6b38">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-palette-2-light-2 u-shape u-shape-rectangle u-shape-1"></div>
        <div class="u-clearfix u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-container-style u-image u-layout-cell u-right-cell u-size-40 u-image-1">
                        <div class="u-container-layout"></div>
                    </div>
                    <div class="u-align-right u-container-style u-layout-cell u-left-cell u-size-20 u-layout-cell-2">
                        <div class="u-container-layout u-valign-middle-lg u-valign-middle-md u-valign-middle-xl">
                            <h4 class="u-custom-font u-text u-text-palette-2-light-1 u-text-1">Hello! Hola! Bonjour!</h4>
                            <h2 class="u-custom-font u-text u-text-2">I work with you to create a business that you're proud of...all while maximizing your profits + giving you (more) freedom.</h2>
                            <p class="u-text u-text-3">Clients and partners often cite ability to create, curate, and cultivate culturally appropriate campaigns. “I grew up in the inner city with a single mom and am largely self-made. I know how to speak to the young guy on the corner, and to the boss in the boardroom.</p>
                            <a href="https://nicepage.com/templates" class="u-border-2 u-border-palette-2-dark-2 u-btn u-button-style u-btn-1">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>