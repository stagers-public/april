<section class="u-clearfix u-section-6" id="sec-f2f0">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-center u-container-style u-expanded-width-sm u-group u-group-1">
            <div class="u-container-layout u-valign-middle u-container-layout-1">
                <h2 class="u-custom-font u-text u-text-1">How﻿﻿﻿ We Can ﻿﻿﻿Work﻿﻿﻿ ﻿﻿﻿﻿﻿﻿Closer﻿﻿﻿﻿﻿﻿...</h2>
            </div>
        </div>
        <div class="u-clearfix u-expanded-width u-gutter-20 u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-container-style u-layout-cell u-left-cell u-size-20 u-size-20-md u-layout-cell-1">
                        <div class="u-container-layout">
                            <img class="u-expanded-width u-image u-image-1" src="assets/images/40d507af16b2432c7c0369fd598ccede.jpeg">
                            <h3 class="u-custom-font u-text u-text-2">Done-for-You</h3>
                            <p class="u-text u-text-3">Sample text. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                    <div class="u-container-style u-layout-cell u-size-20 u-size-20-md u-layout-cell-2">
                        <div class="u-container-layout">
                            <img class="u-expanded-width u-image u-image-2" src="assets/images/bb116f074b1e9011e76cfd5f1e161fb4.jpeg">
                            <h3 class="u-custom-font u-text u-text-4">Turbo Jet</h3>
                            <p class="u-text u-text-5">Sample text. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                    <div class="u-container-style u-layout-cell u-right-cell u-size-20 u-size-20-md u-layout-cell-3">
                        <div class="u-container-layout">
                            <img class="u-expanded-width u-image u-image-3" src="assets/images/a1912665684d9eacdb28c882d993a07d.jpeg">
                            <h3 class="u-custom-font u-text u-text-6">First Class</h3>
                            <p class="u-text u-text-7">Sample text. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>