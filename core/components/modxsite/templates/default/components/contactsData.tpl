<div class="u-size-30">
    <div class="u-layout-col">
        <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
            <div class="u-container-layout u-valign-middle u-container-layout-1">
                <h2 class="u-text u-text-1">Empire State Building</h2>
                <p class="u-text u-text-2">350 5th Ave, New York, NY 10118</p>
                <p class="u-text u-text-3">1 212-736-3100</p>
                <p class="u-text u-text-4">
                    <a href="mailto:contacts@esbnyc.com">contacts@esbnyc.com</a>
                </p>
            </div>
        </div>
        <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-2">
            <div class="u-container-layout u-valign-middle u-container-layout-2">
                <h2 class="u-custom-font u-text u-text-5">Be In-The-Know NOW to Save Stress Later!</h2>
                <p class="u-text u-text-6">A graduate of the Goldman Sachs 10,000 Small Businesses program, Towan funnels her decades-long experience in corporate consulting, marketing, and public relations into her personal brand, TowanIsom.com, and helping her clients.</p>
            </div>
        </div>
    </div>
</div>