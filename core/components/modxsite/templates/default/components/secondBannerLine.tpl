<section class="u-clearfix u-palette-2-light-3 u-section-5" id="carousel_33f5">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-left-lg u-align-left-md u-align-left-xl u-align-left-xs u-palette-2-light-2 u-shape u-shape-rectangle u-shape-1"></div>
        <div class="u-clearfix u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-align-left u-container-style u-layout-cell u-left-cell u-size-20 u-layout-cell-1">
                        <div class="u-container-layout u-container-layout-1">
                            <p class="u-text u-text-1">A graduate of the Goldman Sachs 10,000 Small Businesses program, Towan funnels her decades-long experience in corporate consulting, marketing, and public relations into her personal brand, TowanIsom.com, and helping her clients. In hopes that it will inspire and ignite women and young professionals to make their career goals and professional dreams a reality.</p>
                        </div>
                    </div>
                    <div class="u-container-style u-image u-layout-cell u-right-cell u-size-40 u-image-1">
                        <div class="u-container-layout u-container-layout-2"></div>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="u-custom-font u-text u-text-2">It doesn’t take tons of time or scary talent to be successful. You’ve already got what it takes. Now, you need to tap it. Let’s go!</h2>
    </div>
</section>