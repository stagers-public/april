<section class="u-clearfix u-palette-2-light-2 u-section-2" id="sec-2a65">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-center u-container-style u-expanded-width-sm u-group u-group-1">
            <div class="u-container-layout u-valign-middle u-container-layout-1">
                <h2 class="u-custom-font u-text u-text-body-alt-color u-text-1">
                    {include 'common/content.tpl'}
                </h2>
            </div>
        </div>
    </div>
</section>