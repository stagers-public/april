<section class="u-clearfix u-section-8" id="sec-ad18">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-center u-container-style u-expanded-width-sm u-group u-group-1">
            <div class="u-container-layout u-valign-middle u-container-layout-1">
                <h2 class="u-custom-font u-text u-text-1">What our customers say</h2>
                <p class="u-text u-text-2">Sample text. Lorem ipsum dolor sit amet, consectetur adipiscing elit nullam nunc justo sagittis suscipit ultrices.</p>
            </div>
        </div>
        <div class="u-clearfix u-expanded-width-lg u-expanded-width-md u-expanded-width-xl u-expanded-width-xs u-gutter-20 u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-align-center-lg u-align-center-md u-align-center-xs u-container-style u-layout-cell u-left-cell u-size-30 u-layout-cell-1">
                        <div class="u-container-layout u-valign-middle-xs u-valign-top-lg u-valign-top-md u-valign-top-xl u-container-layout-2">
                            <div alt="" class="u-align-left u-image u-image-circle u-image-1" data-image-width="450" data-image-height="520"></div>
                            <p class="u-align-center-sm u-align-center-xl u-text u-text-3">"Aenean pulvinar dui ornare, feugiat lorem non, ultrices justo. Mauris efficitur, mauris in auctor euismod, quam elit ultrices urna, eget eleifend arcu risus id metus. Maecenas ex enim, mattis eu velit vitae, blandit mattis sapien. Sed aliquam leo et semper vestibulum."</p>
                            <h5 class="u-align-center-sm u-align-center-xl u-custom-font u-text u-text-4">Bertie Norton</h5>
                            <h6 class="u-align-center-lg u-align-center-sm u-align-center-xl u-text u-text-5">Secretary</h6>
                        </div>
                    </div>
                    <div class="u-align-center-sm u-align-center-xs u-container-style u-layout-cell u-right-cell u-size-30 u-layout-cell-2">
                        <div class="u-container-layout u-valign-middle-sm u-valign-middle-xs u-valign-top-lg u-valign-top-xl u-container-layout-3">
                            <div alt="" class="u-align-left u-image u-image-circle u-image-2" data-image-width="450" data-image-height="520"></div>
                            <p class="u-align-center-lg u-align-center-md u-align-center-xl u-text u-text-6">"Aenean pulvinar dui ornare, feugiat lorem non, ultrices justo. Mauris efficitur, mauris in auctor euismod, quam elit ultrices urna, eget eleifend arcu risus id metus. Maecenas ex enim, mattis eu velit vitae, blandit mattis sapien. Sed aliquam leo et semper vestibulum."</p>
                            <h5 class="u-align-center-lg u-align-center-md u-align-center-xl u-custom-font u-text u-text-7">Nat Reynolds</h5>
                            <h6 class="u-align-center-lg u-align-center-md u-align-center-xl u-text u-text-8">Accountant-auditor</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>