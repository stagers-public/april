<section class="u-clearfix u-section-4" id="sec-b36a">
    <div class="u-clearfix u-sheet u-sheet-1">
        <div class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xl u-container-style u-group u-group-1">
            <div class="u-container-layout u-valign-middle-lg u-valign-middle-md u-valign-middle-sm u-valign-middle-xl u-container-layout-1">
                <h2 class="u-align-center-xs u-custom-font u-text u-text-1">Yes, you can have it all!</h2>
                <div class="u-border-7 u-border-grey-dark-1 u-line u-line-horizontal u-line-1"></div>
            </div>
        </div>
        <div class="u-clearfix u-expanded-width u-gutter-20 u-layout-wrap u-layout-wrap-1">
            <div class="u-layout">
                <div class="u-layout-row">
                    <div class="u-container-style u-image u-layout-cell u-left-cell u-size-20 u-size-20-md u-image-1">
                        <div class="u-container-layout"></div>
                    </div>
                    <div class="u-container-style u-image u-layout-cell u-size-20 u-size-20-md u-image-2">
                        <div class="u-container-layout"></div>
                    </div>
                    <div class="u-container-style u-image u-layout-cell u-right-cell u-size-20 u-size-20-md u-image-3">
                        <div class="u-container-layout"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>