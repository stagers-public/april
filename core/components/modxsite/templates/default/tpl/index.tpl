{extends 'layout.tpl'}

{block 'custom'}
    {$modx->regClientCSS('assets/web/styles/top.css', 'all')}
{/block}

{* header *}
{block 'header'}
    {include 'common/header.tpl'}
{/block}


{* content *}
{block 'content'}

    {* first banner line *}
    {include 'components/firstBannerLine.tpl'}

    {* say Hello container *}
    {include 'components/sayHello.tpl'}

    {* you can get it *}
    {include 'components/canHaveIt.tpl'}

    {* second banner line *}
    {include 'components/secondBannerLine.tpl'}

    {* how we work *}
    {include 'components/howWeWork.tpl'}

    {* counters *}
    {include 'components/counters.tpl'}

    {* reviews *}
    {include 'components/reviews.tpl'}

{/block}