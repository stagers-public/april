{extends 'layout.tpl'}

{block 'content'}

    {* banner line *}
    {include 'components/firstBannerLine.tpl'}

    <section class="u-clearfix u-palette-2-light-3 u-section-9" id="sec-9e83">
        <div class="u-clearfix u-sheet u-sheet-1">
            <div class="u-clearfix u-expanded-width u-gutter-0 u-layout-spacing-vertical u-layout-wrap u-layout-wrap-1">
                <div class="u-gutter-0 u-layout">
                    <div class="u-layout-row">
                        {include 'components/contactsData.tpl'}
                        {include 'components/map.tpl'}
                    </div>
                </div>
            </div>
        </div>
    </section>

{/block}