<?php

class modWebMenuGetdataProcessor extends modProcessor
{

    protected $activeIDs = array(); // ID of active parents

    public function initialize()
    {

        $this->setDefaultProperties(array(
            'id' => 'menu', // Menu id
            'cacheable' => false,
            'startId' => $this->modx->resource->id,
            'level' => 1,
            'sortBy' => 'menuindex',
            'sortOrder' => 'ASC',
            'levelClass' => '',
            'activeClass' => 'active',
            'ignoreHidden' => false,
            'showUnpublished' => false,
        ));

        return parent::initialize();
    }

    public function process()
    {
        $output = '';

// get active parents
        if (!empty($this->modx->resource) AND $this->modx->resource instanceOf modResource) {
            $resource = $this->modx->resource;
            $this->activeIDs[] = $resource->id;

            while ($resource = $resource->getOne('Parent')) {
                $this->activeIDs[] = $resource->id;
            }
        }

// get menu items
        if (!$items = $this->getMenuItems()) {
            return;
        }

// prepare menu items
        $items = $this->prepareMenu($items);

        return array(
            'success' => true,
            'message' => '',
            'object' => $items,
        );
    }

    public function getMenuItems()
    {
        $items = array();

        $startId = $this->getProperty('startId');
        $level = $this->getProperty('level');
        $cacheable = $this->getProperty('cacheable');
        $id = $this->getProperty('id', 'menu');
        $cacheKey = $this->modx->context->key . "/{$id}/{$startId}";

        if ($cacheable) {
            if ($fromCache = $this->modx->cacheManager->get($cacheKey)) {
                return $fromCache;
            }
        }

//else
        if ($items = $this->getItems($startId, $level)) {
            if ($cacheable) {
                $this->modx->cacheManager->set($cacheKey, $items);
            }
        }

        return $items;
    }

    protected function getItems($parent, $level)
    {
        $level--;
        $items = array();
        $q = $this->modx->newQuery('modResource');

        $where = $this->getDefaultConditions();

        $where['parent'] = $parent;

        $q->where($where);

        $q->select(array(
            'id', 'parent', 'pagetitle', 'longtitle', 'description', 'menutitle', 'link_attributes', 'uri', 'alias',
        ));
        $q->sortby($this->getProperty('sortBy'), $this->getProperty('sortOrder'));
        if ($q->prepare() && $q->stmt->execute()) {
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                if ($level > 0) {
                    $row['childs'] = $this->getItems($row['id'], $level);
                } else {
                    $row['childs'] = array();
                }
                $items[$row['id']] = $row;
            }
        }
        return $items;
    }

    protected function prepareMenu(array & $items, $currentlevel = 1)
    {
        $levelClass = $this->getProperty('levelClass');
        $activeClass = $this->getProperty('activeClass');

        foreach ($items as &$item) {

            $cls = array();

            if ($levelClass) {
                $cls[] = "{$levelClass}{$currentlevel}";
            }

            $item['linktext'] = ($item['menutitle'] ? $item['menutitle'] : $item['pagetitle']);

            if (in_array($item['id'], $this->activeIDs)) {
                if ($activeClass) {
                    $cls[] = $activeClass;
                }
            }

            $item['cls'] = implode(" ", $cls);

            if ($item['childs']) {
                $item['childs'] = $this->prepareMenu($item['childs'], $currentlevel + 1);
            }
        }

        return $items;
    }

    protected function getDefaultConditions()
    {
        $where = array(
            'deleted' => 0,
        );

        if (!$this->getProperty('showUnpublished')) {
            $where['published'] = true;
        }

        if (!$this->getProperty('ignoreHidden')) {
            $where['hidemenu'] = false;
        }

        if ($_where = $this->getProperty('where')) {
            $where = array_merge($where, $_where);
        }
        return $where;
    }
}


return 'modWebMenuGetdataProcessor';