const form = document.querySelector('.u-content-data')
const formSubmit = form.querySelector('.submit')
const formFields = form.getElementsByTagName('input')

const result = document.createElement('div')
result.classList.add('result')
form.appendChild(result)


formSubmit.addEventListener('click', (ev) => {
    ev.preventDefault()

    const data = Object.assign([], formFields).reduce((acc, arg) => {
        if (arg.hasAttribute('name')) {
            acc = acc * arg.value
            return acc
        } else {
            return acc
        }
    }, 1)

    const result = form.querySelector('.result')
    result.textContent = data !== 0 ? data : 'Ошибка в форме'
    result.replaceChild(data, result)

})